def call (String pseudo) {

	def store = new PseudoStore();
	def fullName = store.findNameByPseudo(pseudo)
	
	if(fullName)
		echo "Trouve ! => Tu es ${fullName}"
	else
		echo 'Pseudo non reconnu'
}
