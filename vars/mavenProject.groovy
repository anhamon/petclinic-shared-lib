def call () {

pipeline {

    agent any

    stages {
	
        stage('verify') {
            steps {
                sh './mvnw -v'
                quiEsTu "twitter"
            }
		}
        stage('compile') {
            steps {
                sh './mvnw clean compile'
            }
		}
        stage('test') {
            steps {
                sh './mvnw test'
            }

            post {
                always {
                    junit 'target/surefire-reports/*.xml'
				}	
            }
        }
        stage('quality') {
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh './mvnw sonar:sonar -Pcoverage'
            }
        }
    }
}
}
}
